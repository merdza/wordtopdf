﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading;
using Word = NetOffice.WordApi;
using NetOffice;
using NetOffice.OfficeApi.Tools;
using NetOffice.WordApi.Enums;

namespace WordToPdf
{
    public class WordDocument
    {
        public string FullName { get; set; }

        public string FolderPath { get; set; }
        public string FileName { get; set; }

        public string PdfSaveDocumentAsFullPath { get; set; }

        protected static object locker = new object();
        protected static Mutex mutex = new Mutex();

        public WordDocument()
        {
            FullName = string.Empty;
        }
        
        public WordDocument(string fullName)
        {
            FullName = fullName;
        }

        public WordDocument(string folderPath, string fileName)
        {
            FullName = Path.Combine(folderPath, fileName);
        }

        protected virtual void CreateDocumentName(bool savePdfAlso)
        {
            //ovde se definise ime dokumenta u opstem slucaju. Ako treba da se promeni, overriduje se metoda u izvedenim klasama
            DateTime now = DateTime.Now;
            string timeStamp = now.Year + "-" + now.Month + "-" + now.Day + "-" + now.Hour + "-" + now.Minute + "-" + now.Second + "-" + now.Millisecond;

            //todo procitaj putanje iz baze
            //folder gde se snima word fajl
            string wordSaveAsFolder = @"C:\Templates";
            string wordSaveAsFileName = @"tempWord" + timeStamp + @".doc";
            FullName = Path.Combine(wordSaveAsFolder, wordSaveAsFileName);
            
            if (savePdfAlso)
            {
                //ako treba da se snimi i pdf fajl pored word fajla
                //folder gde se snima pdf fajl
                string pdfSaveAsFolder = @"C:\Templates";
                string pdfSaveAsFileName = @"tempPdf" + timeStamp + @".pdf";
                PdfSaveDocumentAsFullPath = Path.Combine(pdfSaveAsFolder, pdfSaveAsFileName);
            }
        }

        public void CreatePdfDocument()
        {
            if (mutex.WaitOne(10000))  //ovo moze da radi samo jedan po jedan. Blokiraj maksimalno 10 sekundi
            {
                Word.Application wordApp = null;
                string tempTemplateFile = string.Empty; //privremeni template fajl sa kojim cu da radim da se slucajno ne bi nesto desilo pravom template fajlu

                try
                {
                    wordApp = new Word.Application
                    {
                        DisplayAlerts = WdAlertLevel.wdAlertsNone
                    };

                    object wordDocumentObj = (object)FullName;

                    Word.Document doc = wordApp.Documents.Add(wordDocumentObj);

                    doc.ExportAsFixedFormat(PdfSaveDocumentAsFullPath, Word.Enums.WdExportFormat.wdExportFormatPDF);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error creating documents. " + ex.Message);
                }
                finally
                {
                    // close word and dispose reference
                    wordApp.Quit();
                    wordApp.Dispose();
                    mutex.ReleaseMutex();
                }
            }
            else
            {
                throw new Exception("Thread busy. Document was not created.");
            }
        }

        
    }
}
