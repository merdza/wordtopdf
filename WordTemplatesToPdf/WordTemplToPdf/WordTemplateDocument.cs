﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Word = NetOffice.WordApi;
using NetOffice.WordApi.Enums;

namespace WordToPdf
{
    public class WordTemplateDocument : WordDocument
    {
        //ova klasa predstavlja word dokument koji je napravljen od nekog template-a
        //samim tim, pored podataka o word dokumentu koji se nasleđuju, sadrži i dokument koji predstavlja sam template od kog se pravi
        //kao i listu bukmarka koji se nalaze u template-u i koji bi trebalo da se popune

        public WordDocument Template { get; set; }  //template od kog se pravi dokument
        public List<Bookmark> Bookmarks { get; set; }  //bukmarci koji se nalaze u template dokumentu


        public WordTemplateDocument()
            : base()
        {
            Template = new WordDocument();
            Bookmarks = new List<Bookmark>();
        }

        public WordTemplateDocument(string fullName, WordDocument template, List<Bookmark> bookmarks)
            : base(fullName)
        {
            Template = template;
            Bookmarks = bookmarks;
            CreateWordDocumentFromTemplate();
        }

        public WordTemplateDocument(string folderPath, string fileName, WordDocument template, List<Bookmark> bookmarks)
            : base(folderPath, fileName)
        {
            Template = template;
            Bookmarks = bookmarks;
            CreateWordDocumentFromTemplate();
        }

        //method to put frame on one cell of a word table
        protected void SetTableBorder(Word.Document doc, 
                                    int tableIndex, 
                                    int colIndex, 
                                    int rowIndex,
                                    Word.Enums.WdColor borderColor,
                                    Word.Enums.WdLineWidth borderLineWidth
                                    )
        {
            Word.Table table = doc.Tables[tableIndex];

            table.Cell(rowIndex, colIndex).Range.Borders.OutsideLineWidth = borderLineWidth;
            table.Cell(rowIndex, colIndex).Range.Borders.OutsideColor = borderColor;
        }

        //method for adding new row to a word table
        protected void AddTableRow(Word.Document doc,
                                    int tableIndex,
                                    List<string> rowData,
                                    float fontSize,
                                    int boldFont
                                    )
        {
            try
            {
                Word.Table table = doc.Tables[tableIndex];
                if (table == null)
                {
                    throw new Exception("Selecting table in word document failed.");
                }

                object oMissing = System.Reflection.Missing.Value;

                Word.Row newRow = table.Rows.Add(/*ref oMissing*/);
                if (newRow == null)
                {
                    throw new Exception("Creating new row in a table failed.");
                }

                int col = 1;
                foreach (var item in rowData)
                {
                    newRow.Cells[col].Range.Bold = boldFont;
                    newRow.Cells[col].Range.Font.Size = fontSize;
                    newRow.Cells[col++].Range.Text = item;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("AddTableRow greska: " + ex.Message + "||||" + ex.StackTrace + "||||" + ex.InnerException + "||||" + ex.Source + "||||" + ex.ToString());
            }
        }

        public virtual string CreateWordDocumentFromTemplate()
        {
            if (mutex.WaitOne(10000))  //ovo moze da radi samo jedan po jedan. Blokiraj maksimalno 10 sekundi
            {
                Word.Application wordApp = null;
                
                try
                {
                    wordApp = new Word.Application
                    {
                        DisplayAlerts = WdAlertLevel.wdAlertsNone
                    };

                    object wordTemplateDocumentObj = (object)Template.FullName;

                    Word.Document doc = wordApp.Documents.Add(wordTemplateDocumentObj);

                    //ubaci bookmark-e u dokument
                    foreach (Bookmark bkm in Bookmarks)
                    {
                        if (doc.Bookmarks.Exists(bkm.Name))
                        {
                            (doc.Bookmarks.First(item => item.Name == bkm.Name)).Select();
                            wordApp.Selection.TypeText(bkm.Value);
                        }
                    }

                    doc.SaveAs(FullName);

                    if (PdfSaveDocumentAsFullPath != "")
                    {
                        doc.ExportAsFixedFormat(PdfSaveDocumentAsFullPath, Word.Enums.WdExportFormat.wdExportFormatPDF);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error creating documents. " + ex.Message);
                }
                finally
                {
                    // close word and dispose reference
                    wordApp.Quit();
                    wordApp.Dispose();
                    mutex.ReleaseMutex();
                }

                if (PdfSaveDocumentAsFullPath != "")
                {
                    //ako je snimljen pdf, vrati njega kao rezultat
                    return PdfSaveDocumentAsFullPath;
                }
                else
                {
                    //ako nije snimljen pfd, vrati link ka word dokumentu
                    return FullName;
                }
            }
            else
            {
                throw new Exception("Thread busy. Document was not created.");
            }
        }

        protected Word.Bookmark GetBookmarkByName(Word.Bookmarks bookmarks, string bookmarkName)
        {
            foreach (Word.Bookmark item in bookmarks)
            {
                if (item.Name.Equals(bookmarkName, StringComparison.InvariantCultureIgnoreCase))
                {
                    return item;
                }
            }
            return null;
        }
    }

    
}
