﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WordToPdf
{
    public class Bookmark
    {
        public string Name { get; set; }
        public string Value { get; set; }

        public Bookmark(string n, string v)
        {
            Name = n;
            Value = v;
        }
    }
}
