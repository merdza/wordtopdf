﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WordToPdf;

namespace WordToPdfTest
{
    public class TemplateDocumentSerbianPost : WordTemplateDocument
    {
        public TemplateDocumentType DocumentType { get; set; }
        public string ContractDateTime { get; set; }

        protected virtual string GetContractDateTime(string requestNumber)
        {
            DateTime now = DateTime.Now;
            string timeStamp = Utils.GetShortSerbianDateNoDots(now);
            return requestNumber + "-" + timeStamp;
        }

        protected void SetTemplate()
        {
            //ova metoda treba da procita iz baze putanju do template-a za DocumentType i da postavi Template.FullName
            //todo procitaj iz baze

            //za sada ovako...
            switch (DocumentType)
            {
                case TemplateDocumentType.IssuingIndividual:
                    Template.FullName = @"C:\Git\PisMess\Templates\Templates\Zahtev za izdavanje KV sertifikata.dot";
                    break;
                case TemplateDocumentType.PaymentOrder:
                    Template.FullName = @"C:\Git\PisMess\Templates\Templates\Nalog za placanje template.dot";
                    break;
                case TemplateDocumentType.LegalEntityContractAttachment:
                    Template.FullName = @"C:\Git\PisMess\Templates\Templates\Ugovor za pravno lice za kvalifikovane sertifikate Prilog.dot";
                    break;
                case TemplateDocumentType.Cetvrti:
                    Template.FullName = @"";
                    break;
                case TemplateDocumentType.Peti:
                    Template.FullName = @"";
                    break;
                default:
                    Template.FullName = @"";
                    break;
            }
        }

        protected virtual string GetWordSaveAsPath()
        {
            //ova metoda treba da procita iz baze putanju gde treba da se snima word dokument za odabredjeni DocumentType
            //todo procitaj iz baze

            //za sada ovako...
            string ret = string.Empty;
            switch (DocumentType)
            {
                case TemplateDocumentType.IssuingIndividual:
                    ret = @"C:\Git\PisMess\Templates";
                    break;
                case TemplateDocumentType.PaymentOrder:
                    ret = @"C:\Git\PisMess\Templates";
                    break;
                case TemplateDocumentType.LegalEntityContractAttachment:
                    ret = @"C:\Git\PisMess\Templates";
                    break;
                case TemplateDocumentType.Cetvrti:
                    ret = @"C:\Git\PisMess\Templates";
                    break;
                case TemplateDocumentType.Peti:
                    ret = @"C:\Git\PisMess\Templates";
                    break;
                default:
                    ret = @"C:\Git\PisMess\Templates";
                    break;
            }
            return ret;
        }

        protected virtual string GetWordSaveAsFileName()
        {
            //ova metoda treba da procita iz baze fiksi deo imena word dokumenta za odabredjeni DocumentType
            //todo procitaj iz baze

            //za sada ovako...
            string ret = string.Empty;
            switch (DocumentType)
            {
                case TemplateDocumentType.IssuingIndividual:
                    ret = @"Zahtev za izdavanje kvalifikovanog sertifikata-";
                    break;
                case TemplateDocumentType.PaymentOrder:
                    ret = @"Nalog za uplatu-";
                    break;
                case TemplateDocumentType.LegalEntityContractAttachment:
                    ret = @"Prilog Ugovora za kvalifikovane sertifikate-";
                    break;
                case TemplateDocumentType.Cetvrti:
                    ret = @"xxx";
                    break;
                case TemplateDocumentType.Peti:
                    ret = @"xxx";
                    break;
                default:
                    ret = @"xxx";
                    break;
            }
            return ret;
        }

        protected virtual string GetPdfSaveAsPath()
        {
            //ova metoda treba da procita iz baze putanju gde treba da se snima pdf dokument za odabredjeni DocumentType
            //todo procitaj iz baze

            //za sada ovako...
            string ret = string.Empty;
            switch (DocumentType)
            {
                case TemplateDocumentType.IssuingIndividual:
                    ret = @"C:\Git\PisMess\Templates";
                    break;
                case TemplateDocumentType.PaymentOrder:
                    ret = @"C:\Git\PisMess\Templates";
                    break;
                case TemplateDocumentType.LegalEntityContractAttachment:
                    ret = @"C:\Git\PisMess\Templates";
                    break;
                case TemplateDocumentType.Cetvrti:
                    ret = @"C:\Git\PisMess\Templates";
                    break;
                case TemplateDocumentType.Peti:
                    ret = @"C:\Git\PisMess\Templates";
                    break;
                default:
                    ret = @"C:\Git\PisMess\Templates";
                    break;
            }
            return ret;
        }

        protected virtual string GetPdfSaveAsFileName()
        {
            //ova metoda treba da procita iz baze fiksi deo imena pdf dokumenta za odabredjeni DocumentType
            //todo procitaj iz baze

            //za sada ovako...
            string ret = string.Empty;
            switch (DocumentType)
            {
                case TemplateDocumentType.IssuingIndividual:
                    ret = @"Zahtev za izdavanje kvalifikovanog sertifikata-";
                    break;
                case TemplateDocumentType.PaymentOrder:
                    ret = @"Nalog za uplatu-";
                    break;
                case TemplateDocumentType.LegalEntityContractAttachment:
                    ret = @"Prilog Ugovora za kvalifikovane sertifikate-";
                    break;
                case TemplateDocumentType.Cetvrti:
                    ret = @"xxx";
                    break;
                case TemplateDocumentType.Peti:
                    ret = @"xxx";
                    break;
                default:
                    ret = @"xxx";
                    break;
            }
            return ret;
        }

        protected override void CreateDocumentName(bool savePdfAlso)
        {
            //najcesce se za Postu imena dokumenata sastoje od fiksnog dela i kombinacije broja zahteva i datuma (ContractDateTime)
            //a ako nije tako, redefinisacemo ovu metodu u izvedenoj klasi

            //folder gde se snima word fajl
            string wordSaveAsFolder = GetWordSaveAsPath();
            string wordSaveAsFileName = GetWordSaveAsFileName() + ContractDateTime + @".doc";
            FullName = Path.Combine(wordSaveAsFolder, wordSaveAsFileName);

            if (savePdfAlso)
            {
                //ako treba da se snimi i pdf fajl pored word fajla
                //folder gde se snima pdf fajl
                string pdfSaveAsFolder = GetPdfSaveAsPath();
                string pdfSaveAsFileName = GetPdfSaveAsFileName() + ContractDateTime + @".pdf";
                PdfSaveDocumentAsFullPath = Path.Combine(pdfSaveAsFolder, pdfSaveAsFileName);
            }
        }
    }
}
