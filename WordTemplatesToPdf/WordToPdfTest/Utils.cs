﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordToPdfTest
{
    public static class Utils
    {
        public static void GetCultureNames()
        {
            // ispisi u konzolu kodove svih kulture
            List<string> list = new List<string>();
            foreach (CultureInfo ci in CultureInfo.GetCultures(CultureTypes.AllCultures))
            {
                string specName = "(none)";
                try { specName = CultureInfo.CreateSpecificCulture(ci.Name).Name; }
                catch { }
                list.Add(String.Format("{0,-12}{1,-12}{2}", ci.Name, specName, ci.EnglishName));
            }

            list.Sort();  // sort by name

            // write to console
            Console.WriteLine("CULTURE   SPEC.CULTURE  ENGLISH NAME");
            Console.WriteLine("--------------------------------------------------------------");
            foreach (string str in list)
                Console.WriteLine(str);
        }

        public static string GetMonthString(int m)
        {
            string ret = "";
            //switch (m)
            //{
            //    case 1:
            //        ret = guiLang.Utils_Month_Jan;
            //        break;
            //    case 2:
            //        ret = guiLang.Utils_Month_Feb;
            //        break;
            //    case 3:
            //        ret = guiLang.Utils_Month_Mar;
            //        break;
            //    case 4:
            //        ret = guiLang.Utils_Month_Apr;
            //        break;
            //    case 5:
            //        ret = guiLang.Utils_Month_May;
            //        break;
            //    case 6:
            //        ret = guiLang.Utils_Month_Jun;
            //        break;
            //    case 7:
            //        ret = guiLang.Utils_Month_Jul;
            //        break;
            //    case 8:
            //        ret = guiLang.Utils_Month_Aug;
            //        break;
            //    case 9:
            //        ret = guiLang.Utils_Month_Sep;
            //        break;
            //    case 10:
            //        ret = guiLang.Utils_Month_Oct;
            //        break;
            //    case 11:
            //        ret = guiLang.Utils_Month_Nov;
            //        break;
            //    case 12:
            //        ret = guiLang.Utils_Month_Dec;
            //        break;
            //    case 13:
            //        ret = guiLang.Utils_Month_Jan;
            //        break;
            //}
            return ret;
        }

        public static string AddLeadingZeros(int i, int totalNumbers)
        {
            try
            {
                int mul = 10;
                string lead = "";
                for (int j = totalNumbers; j > 1; j--)
                {
                    if (i < mul)
                    {
                        for (int k = 0; k < j - 1; k++)
                        {
                            lead += "0";
                        }
                        return lead + i.ToString();
                    }
                    mul *= 10;
                }
                return i.ToString();
            }
            catch (Exception)
            {
                //ma ovo nema sanse da se desi
                //try catch je samo da ne pukne program
                return "0";
            }
        }

        public static string MakeFixedLengthString(string s, int length)
        {
            //koristi se za pravljenje stringa fiksne duzine koji se npr. koristi za racune
            //ako je string kraci od potrebne duzine, dodaju mu se spejsovi
            //ako je duzi, skracuje se na potrebnu duzinu
            try
            {
                if (s.Length < length)
                {
                    int len = s.Length;
                    for (int i = 0; i < length - len; i++)
                    {
                        s += " ";
                    }
                }
                else if (s.Length > length)
                {
                    s = s.Substring(0, length);
                }
                return s;
            }
            catch (Exception)
            {
                //ma ovo nema sanse da se desi
                //try catch je samo da ne pukne program
                return "0";
            }
        }

        //ex 15052018
        public static string GetShortSerbianDateNoDots(DateTime dt)
        {
            return Utils.AddLeadingZeros(dt.Day, 2) + Utils.AddLeadingZeros(dt.Month, 2) + Utils.AddLeadingZeros(dt.Year, 2);
        }

        //ex 15.05.2018. ili bez tacke na kraju
        public static string GetShortSerbianDate(DateTime dt, bool dotAfterYear)
        {
            string ret = Utils.AddLeadingZeros(dt.Day, 2) + @"." + Utils.AddLeadingZeros(dt.Month, 2) + @"." + Utils.AddLeadingZeros(dt.Year, 2);
            if (dotAfterYear)
            {
                ret += @".";
            }
            return ret;
        }

        public static string CreateTempFile()
        {
            string fileName = string.Empty;

            try
            {
                // Get the full name of the newly created Temporary file. 
                fileName = Path.GetTempFileName();

                // Craete a FileInfo object to set the file's attributes
                FileInfo fileInfo = new FileInfo(fileName)
                {
                    // Set the Attribute property of this file to Temporary. 
                    // Although this is not completely necessary, the .NET Framework is able 
                    // to optimize the use of Temporary files by keeping them cached in memory.
                    Attributes = FileAttributes.Temporary
                };

            }
            catch (Exception ex)
            {
                throw new Exception("Unable to create TEMP file or set its attributes: " + ex.Message);
            }

            return fileName;
        }

        public static void DeleteFile(string file)
        {
            try
            {
                if (File.Exists(file))
                {
                    File.Delete(file);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Deleting file " + file + " failed. " + ex.Message);
            }
        }
    }
}
