﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WordToPdfTest
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public List<string> CreateDocument(TemplateDocumentType templateDocumentType, List<DocumentParameter> documentParameters)
        {
            List<string> ret = new List<string>();
            try
            {
                //DateTime now = DateTime.Now;
                //string timeStamp = now.Year + "-" + now.Month + "-" + now.Day + "-" + now.Hour + "-" + now.Minute + "-" + now.Second + "-" + now.Millisecond;

                ////todo procitaj putanje iz baze
                ////folder gde se snima word fajl
                //string wordSaveAsFolder = @"C:\Git\PisMess\Templates";
                //string wordSaveAsFileName = @"tempWord" + timeStamp + @".doc";
                //string wordSaveAs = Path.Combine(wordSaveAsFolder, wordSaveAsFileName);

                ////folder gde se snima pdf fajl
                //string pdfSaveAsFolder = @"C:\Git\PisMess\Templates";
                //string pdfSaveAsFileName = @"tempPdf" + timeStamp + @".pdf";
                //string pdfSaveAs = Path.Combine(pdfSaveAsFolder, pdfSaveAsFileName);

                //generisanje word i pdf dokumenta
                //moguce je generisati samo word dokument i tada nije potrebno prosledjivati parametre
                //a ako treba da se generise i pdf, onda su parametri true i puno ime pdf dokumenta koji treba da se generiše
                string createdFilePath = GetTemplateDocumentObject(templateDocumentType, documentParameters).CreateWordDocumentFromTemplate();

                ret.Add("0");
                ret.Add(createdFilePath);


            }
            catch (Exception ex)
            {
                ret.Add(ex.Message);
            }

            return ret;
        }

        private WordToPdf.WordTemplateDocument GetTemplateDocumentObject(TemplateDocumentType templateDocumentType, List<DocumentParameter> documentParameters)
        {
            WordToPdf.WordTemplateDocument ret = null;
            switch (templateDocumentType)
            {
                case TemplateDocumentType.IssuingIndividual:
                    ret = new TemplateDocumentIssuingIndividual(getParameterValues(documentParameters, "requestNumber").ToString(),
                                                                getParameterValues(documentParameters, "firstName").ToString(),
                                                                getParameterValues(documentParameters, "lastName").ToString(),
                                                                getParameterValues(documentParameters, "jmbg").ToString(),
                                                                getParameterValues(documentParameters, "email").ToString(),
                                                                getParameterValues(documentParameters, "street").ToString(),
                                                                getParameterValues(documentParameters, "streetNo").ToString(),
                                                                getParameterValues(documentParameters, "postNo").ToString(),
                                                                getParameterValues(documentParameters, "place").ToString(),
                                                                getParameterValues(documentParameters, "phone").ToString(),
                                                                getParameterValues(documentParameters, "expiry").ToString(),
                                                                getParameterValues(documentParameters, "hwMedium").ToString(),
                                                                true
                                                                );
                    break;
                case TemplateDocumentType.PaymentOrder:
                    ret = new TemplateDocumentPaymentOrder(getParameterValues(documentParameters, "requestNumber").ToString(),
                                                           getParameterValues(documentParameters, "price").ToString(),
                                                           getParameterValues(documentParameters, "firstName").ToString(),
                                                           getParameterValues(documentParameters, "lastName").ToString(),
                                                           getParameterValues(documentParameters, "street").ToString(),
                                                           getParameterValues(documentParameters, "streetNo").ToString(),
                                                           getParameterValues(documentParameters, "postNo").ToString(),
                                                           getParameterValues(documentParameters, "place").ToString(),
                                                           getParameterValues(documentParameters, "purposeOfPayment").ToString()
                        );
                    break;
                case TemplateDocumentType.LegalEntityContractAttachment:
                    ret = new TemplateDocumentLegalEntityContractAttachment(getParameterValues(documentParameters, "requestNumber").ToString(),
                                                                            getParameterValues(documentParameters, "legalEntityName").ToString(),
                                                                            getParameterValues(documentParameters, "price").ToString(),
                                                                            getParameterValues(documentParameters, "contractDateTime").ToString(),
                                                                            (List<CertificatesAuthorizedUser>)(getParameterValues(documentParameters, "certificatesAuthorizedUsers"))
                                                                            );
                    break;
                case TemplateDocumentType.Cetvrti:
                    throw new NotImplementedException();
                    break;
                case TemplateDocumentType.Peti:
                    throw new NotImplementedException();
                    break;
                default:
                    throw new NotImplementedException();
                    break;
            }
            return ret;
        }

        private object getParameterValues(List<DocumentParameter> documentParameters, string parameterName)
        {
            foreach (DocumentParameter docParam in documentParameters)
            {
                if (docParam.ParameterName.Equals(parameterName))
                {
                    return docParam.ParameterValue;
                }
            }
            return string.Empty;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            List<DocumentParameter> documentParameters = new List<DocumentParameter>
            {
                new DocumentParameter(@"requestNumber", @"10160789"),
                new DocumentParameter(@"firstName", @"Željko"),
                new DocumentParameter(@"lastName", @"Joksimović Janković"),
                new DocumentParameter(@"jmbg", @"1234567891234"),
                new DocumentParameter(@"email", @"something@example.com"),
                new DocumentParameter(@"street", @"Knez Mihailova"),
                new DocumentParameter(@"streetNo", @"23"),
                new DocumentParameter(@"postNo", @"11000"),
                new DocumentParameter(@"place", @"Beograd"),
                new DocumentParameter(@"phone", @"064/123-456"),
                new DocumentParameter(@"expiry", @"365"),
                new DocumentParameter(@"hwMedium", @"SMART_CARD"),
                new DocumentParameter(@"purposeOfPayment", @"UNBLOCKING"),  //ISSUING ili UNBLOCKING
                new DocumentParameter(@"legalEntityName", @"ImeNekeFirme d.o.o."),
                new DocumentParameter(@"price", @"1234.00"),
                new DocumentParameter(@"certificatesAuthorizedUsers", new List<CertificatesAuthorizedUser>
                                                                        {
                                                                            new CertificatesAuthorizedUser(@"Željko", @"Joksimović Janković", @"1234567891234", @"something@example.com", @"011/123456", @"USB_TOKEN", @"365"),
                                                                            new CertificatesAuthorizedUser(@"Жељко", @"Јоксимовић", @"1234567891234", @"something@example.com", @"011/123456", @"SMART_CARD_WITH_READER", @"730"),
                                                                            new CertificatesAuthorizedUser(@"Marija", @"Šerifović", @"1234567891234", @"something@example.com", @"011/123456", @"SMART_CARD", @"1462"),
                                                                            new CertificatesAuthorizedUser(@"Марија", @"Шерифовић", @"1234567891234", @"something@example.com", @"011/123456", @"nekaGlupost", @"1825"),
                                                                            new CertificatesAuthorizedUser(@"Željko", @"Joksimović Janković", @"1234567891234", @"something@example.com", @"011/123456", @"USB_TOKEN", @"365")
                                                                        })
        };
            
            List<string> results = new List<string>();

            results = CreateDocument(TemplateDocumentType.IssuingIndividual, documentParameters);

            if (results[0].Equals("0"))
            {
                textBox1.Text = results[1];
            }
            else
            {
                textBox1.Text = results[0];
            }
        }
    }

    public enum TemplateDocumentType
    {
        IssuingIndividual,
        PaymentOrder,
        LegalEntityContractAttachment,
        Cetvrti,
        Peti
    }

    public class DocumentParameter
    {
        public string ParameterName { get; set; }
        public object ParameterValue { get; set; }  //object zato sto mogu da se salju i klase, npr. klasa korisnika koji treba da se doda kao red u tabeli

        public DocumentParameter(string name, object value)
        {
            ParameterName = name;
            ParameterValue = value;
        }

        public DocumentParameter()
        { }
    }
}
