﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WordToPdf;
using Word = NetOffice.WordApi;
using NetOffice.WordApi.Enums;

namespace WordToPdfTest
{
    public class TemplateDocumentIssuingIndividual : TemplateDocumentSerbianPost
    {
        public int ExpiryColumn { get; set; }
        public int HwMediumRow { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Jmbg { get; set; }
        public string Email { get; set; }
        public string Street { get; set; }
        public string StreetNo { get; set; }
        public string PostNo { get; set; }
        public string Place { get; set; }
        public string Phone { get; set; }

        public TemplateDocumentIssuingIndividual(string requestNumber, string firstName, string lastName, string jmbg, string email, string street, string streetNo, string postNo, string place, string phone, string expiry, string hwMedium, bool savePdfAlso = true)
            : base()
        {

            DocumentType = TemplateDocumentType.IssuingIndividual;
            FirstName = firstName;
            LastName = lastName;
            Jmbg = jmbg;
            Email = email;
            Street = street;
            StreetNo = streetNo;
            PostNo = postNo;
            Place = place;
            Phone = phone;
            ContractDateTime = GetContractDateTime(requestNumber);
            ExpiryColumn = GetExpiryColumn(expiry);
            HwMediumRow = GetHwMediumRow(hwMedium);
            if (ExpiryColumn == 0)
            {
                throw new Exception("No valid expiry period provided: " + expiry);
            }
            if (HwMediumRow == 0)
            {
                throw new Exception("No valid hardware medium provided: " + hwMedium);
            }

            CreateDocumentName(savePdfAlso);

            SetTemplate();

            Bookmarks.Add(new Bookmark(@"FirstName", FirstName));
            Bookmarks.Add(new Bookmark(@"LastName", LastName));
            Bookmarks.Add(new Bookmark(@"Jmbg", Jmbg));
            Bookmarks.Add(new Bookmark(@"Email", Email));
            Bookmarks.Add(new Bookmark(@"Street", Street));
            Bookmarks.Add(new Bookmark(@"StreetNo", StreetNo));
            Bookmarks.Add(new Bookmark(@"PostNo", PostNo));
            Bookmarks.Add(new Bookmark(@"Place", Place));
            Bookmarks.Add(new Bookmark(@"Phone", Phone));
            Bookmarks.Add(new Bookmark(@"ContractDateTime", ContractDateTime));
            Bookmarks.Add(new Bookmark(@"ContractDateTime1", ContractDateTime));
        }

        private int GetExpiryColumn(string expiry)
        {
            //potrebno je da se zaokruzi cena u dokumentu na osnovu duzine trajanja i medijuma koji se koristi
            //ova funkcija vraca kolonu koja treba da se zaokruzi
            /*
                1 godina    365
                2 godine    730
                3 godine    1095
                4 godine    1462
                5 godina    1825
             */
            int ret = 0;
            switch (expiry)
            {
                case "365":
                    ret = 2;
                    break;
                case "730":
                    ret = 3;
                    break;
                case "1095":
                    ret = 4;
                    break;
                case "1462":
                    ret = 5;
                    break;
                case "1825":
                    ret = 6;
                    break;
                default:
                    break;
            }
            return ret;
        }

        private int GetHwMediumRow(string hwMedium)
        {
            //potrebno je da se zaokruzi cena u dokumentu na osnovu duzine trajanja i medijuma koji se koristi
            //ova funkcija vraca red koja treba da se zaokruzi
            /*
                USB token    USB_TOKEN
                Smart kartica sa čitačem    SMART_CARD_WITH_READER
                Smart kartica    SMART_CARD
             */
            int ret = 0;
            switch (hwMedium)
            {
                case "USB_TOKEN":
                    ret = 3;
                    break;
                case "SMART_CARD_WITH_READER":
                    ret = 4;
                    break;
                case "SMART_CARD":
                    ret = 5;
                    break;
                default:
                    break;
            }
            return ret;
        }

        public override string CreateWordDocumentFromTemplate()
        {
            if (mutex.WaitOne(10000))  //ovo moze da radi samo jedan po jedan. Blokiraj maksimalno 10 sekundi
            {
                Word.Application wordApp = null;
                string tempTemplateFile = string.Empty; //privremeni template fajl sa kojim cu da radim da se slucajno ne bi nesto desilo pravom template fajlu

                try
                {
                    wordApp = new Word.Application
                    {
                        DisplayAlerts = WdAlertLevel.wdAlertsNone
                    };

                    //kreiraj privremeni fajl i kopiraj template fajl u njega
                    tempTemplateFile = Utils.CreateTempFile();
                    File.Copy(Template.FullName, tempTemplateFile, true);

                    object wordTemplateDocumentObj = (object)tempTemplateFile;

                    Word.Document doc = wordApp.Documents.Add(wordTemplateDocumentObj);

                    //ubaci bookmark-e u dokument
                    foreach (Bookmark bkm in Bookmarks)
                    {
                        if (doc.Bookmarks.Exists(bkm.Name))
                        {
                            (doc.Bookmarks.First(item => item.Name == bkm.Name)).Select();
                            wordApp.Selection.TypeText(bkm.Value);
                        }
                    }

                    //uokviri celiju u tabeli sa cenom u zavisnosti od perioda vazenja i medijuma na kom je sertifikat
                    SetTableBorder(doc, 2, ExpiryColumn, HwMediumRow, Word.Enums.WdColor.wdColorRed, Word.Enums.WdLineWidth.wdLineWidth300pt);

                    doc.SaveAs(FullName);

                    if (PdfSaveDocumentAsFullPath != "")
                    {
                        doc.ExportAsFixedFormat(PdfSaveDocumentAsFullPath, Word.Enums.WdExportFormat.wdExportFormatPDF);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error creating documents. " + ex.Message);
                }
                finally
                {
                    // close word and dispose reference
                    wordApp.Quit();
                    wordApp.Dispose();
                    mutex.ReleaseMutex();
                    Utils.DeleteFile(tempTemplateFile);
                }

                if (PdfSaveDocumentAsFullPath != "")
                {
                    //ako je snimljen pdf, vrati njega kao rezultat
                    return PdfSaveDocumentAsFullPath;
                }
                else
                {
                    //ako nije snimljen pfd, vrati link ka word dokumentu
                    return FullName;
                }
            }
            else
            {
                throw new Exception("Thread busy. Document was not created.");
            }
        }


        


    }
}
