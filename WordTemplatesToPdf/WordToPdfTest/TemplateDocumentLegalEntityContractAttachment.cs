﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using WordToPdf;
using Word = NetOffice.WordApi;
using NetOffice.WordApi.Enums;

namespace WordToPdfTest
{
    public class TemplateDocumentLegalEntityContractAttachment : TemplateDocumentSerbianPost
    {
        public string LegalEntityName { get; set; }
        public string Price { get; set; }
        public List<CertificatesAuthorizedUser> CertificatesAuthorizedUsers { get; set; }

        public TemplateDocumentLegalEntityContractAttachment(string requestNumber, string legalEntityName, string price, string contractDateTime, List<CertificatesAuthorizedUser> users, bool savePdfAlso = true)
            : base()
        {

            DocumentType = TemplateDocumentType.LegalEntityContractAttachment;
            LegalEntityName = legalEntityName;
            Price = price;
            ContractDateTime = GetContractDateTime(requestNumber);
            CertificatesAuthorizedUsers = users;

            CreateDocumentName(savePdfAlso);

            SetTemplate();
            
            Bookmarks.Add(new Bookmark(@"LegalEntityName", LegalEntityName));
            Bookmarks.Add(new Bookmark(@"LegalEntityName1", LegalEntityName));
            Bookmarks.Add(new Bookmark(@"price", Price));
            Bookmarks.Add(new Bookmark(@"ContractDateTime", ContractDateTime));
        }

        public override string CreateWordDocumentFromTemplate()
        {
            if (mutex.WaitOne(10000))  //ovo moze da radi samo jedan po jedan. Blokiraj maksimalno 10 sekundi
            {
                Word.Application wordApp = null;
                string tempTemplateFile = string.Empty; //privremeni template fajl sa kojim cu da radim da se slucajno ne bi nesto desilo pravom template fajlu

                try
                {
                    wordApp = new Word.Application
                    {
                        DisplayAlerts = WdAlertLevel.wdAlertsNone
                    };

                    //kreiraj privremeni fajl i kopiraj template fajl u njega
                    tempTemplateFile = Utils.CreateTempFile();
                    File.Copy(Template.FullName, tempTemplateFile, true);

                    object wordTemplateDocumentObj = (object)tempTemplateFile;

                    Word.Document doc = wordApp.Documents.Add(wordTemplateDocumentObj);

                    //ubaci bookmark-e u dokument
                    foreach (Bookmark bkm in Bookmarks)
                    {
                        if (doc.Bookmarks.Exists(bkm.Name))
                        {
                            (doc.Bookmarks.First(item => item.Name == bkm.Name)).Select();
                            wordApp.Selection.TypeText(bkm.Value);
                        }
                    }

                    //dodaj red u tabelu za svakog ovlascenog korisnika
                    int cnt = 1;
                    foreach (var user in CertificatesAuthorizedUsers)
                    {
                        List<string> rowData = new List<string>
                            {
                                (cnt++).ToString() + ".",
                                user.Name,
                                user.LastName,
                                user.Jmbg,
                                user.Email,
                                user.PhoneNumber,
                                user.Token,
                                user.Card,
                                user.CardWithReader,
                                user.NoMedium,
                                user.Expiry
                            };
                        AddTableRow(doc, 2, rowData, 10, 0);
                    }

                    doc.SaveAs(FullName);

                    if (PdfSaveDocumentAsFullPath != "")
                    {
                        doc.ExportAsFixedFormat(PdfSaveDocumentAsFullPath, Word.Enums.WdExportFormat.wdExportFormatPDF);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error creating documents. " + ex.Message);
                }
                finally
                {
                    // close word and dispose reference
                    wordApp.Quit();
                    wordApp.Dispose();
                    mutex.ReleaseMutex();
                    Utils.DeleteFile(tempTemplateFile);
                }

                if (PdfSaveDocumentAsFullPath != "")
                {
                    //ako je snimljen pdf, vrati njega kao rezultat
                    return PdfSaveDocumentAsFullPath;
                }
                else
                {
                    //ako nije snimljen pfd, vrati link ka word dokumentu
                    return FullName;
                }
            }
            else
            {
                throw new Exception("Thread busy. Document was not created.");
            }
        }


            /*public override string CreateWordDocumentFromTemplate()
            {
                if (mutex.WaitOne(10000))  //ovo moze da radi samo jedan po jedan. Blokiraj maksimalno 10 sekundi
                {
                    Microsoft.Office.Interop.Word.Application wordApp = null;
                    Microsoft.Office.Interop.Word.Document doc = null;

                    string tempTemplateFile = string.Empty; //privremeni template fajl sa kojim cu da radim da se slucajno ne bi nesto desilo pravom template fajlu
                    try
                    {
                        wordApp = new Microsoft.Office.Interop.Word.Application()
                        {
                            Visible = false
                        };

                        //kreiraj privremeni fajl i kopiraj template fajl u njega
                        tempTemplateFile = Utils.CreateTempFile();
                        File.Copy(Template.FullName, tempTemplateFile, true);

                        object wordTemplateDocumentObj = (object)tempTemplateFile;

                        doc = wordApp.Documents.Open(ref wordTemplateDocumentObj);

                        //ubaci bookmark-e u dokument
                        foreach (Bookmark bkm in Bookmarks)
                        {
                            doc.Bookmarks.get_Item(GetBookmarkByName(doc.Bookmarks, bkm.Name)).Range.Text = bkm.Value;
                        }

                        //dodaj red u tabelu za svakog ovlascenog korisnika
                        int cnt = 1;
                        foreach (var user in CertificatesAuthorizedUsers)
                        {
                            List<string> rowData = new List<string>
                            {
                                (cnt++).ToString() + ".",
                                user.Name,
                                user.LastName,
                                user.Jmbg,
                                user.Email,
                                user.PhoneNumber,
                                user.Token,
                                user.Card,
                                user.CardWithReader,
                                user.NoMedium,
                                user.Expiry
                            };
                            AddTableRow(doc, 2, rowData, 10, 0);
                        }

                        doc.SaveAs(FullName);

                        if (PdfSaveDocumentAsFullPath != "")
                        {
                            doc.ExportAsFixedFormat(PdfSaveDocumentAsFullPath, Microsoft.Office.Interop.Word.WdExportFormat.wdExportFormatPDF);
                        }

                        ((Microsoft.Office.Interop.Word._Document)doc).Close(false);
                        ((Microsoft.Office.Interop.Word._Application)(wordApp.Application)).Quit();

                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Error creating documents. " + ex.Message);
                    }
                    finally
                    {
                        try
                        {
                            ReleaseResource(wordApp);
                            ReleaseResource(doc);
                            mutex.ReleaseMutex();
                            Utils.DeleteFile(tempTemplateFile);
                        }
                        catch (Exception)
                        {
                            //nije uspelo oslobadjanje resursa, necu nista da radim
                        }
                    }

                    if (PdfSaveDocumentAsFullPath != "")
                    {
                        //ako je snimljen pdf, vrati njega kao rezultat
                        return PdfSaveDocumentAsFullPath;
                    }
                    else
                    {
                        //ako nije snimljen pfd, vrati link ka word dokumentu
                        return FullName;
                    }
                }
                else
                {
                    throw new Exception("Thread busy. Document was not created.");
                }
            }*/
        }

    //korisnici koji treba da se nadju u tabeli: Podaci o ovlascenim korisnicima sertifikata
    public class CertificatesAuthorizedUser
    {
        public string Name { get; set; }
        public string LastName { get; set; }
        public string Jmbg { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Token { get; set; }
        public string Card { get; set; }
        public string CardWithReader { get; set; }
        public string NoMedium { get; set; }
        public string Expiry { get; set; }

        public CertificatesAuthorizedUser(string name, string lastName, string jmbg, string email, string phoneNumber, string medium, string expiry)
        {
            Name = name;
            LastName = lastName;
            Jmbg = jmbg;
            Email = email;
            PhoneNumber = phoneNumber;
            SetMedium(medium);
            SetExpiry(expiry);
        }

        private void SetMedium(string medium)
        {
            Token = "";
            Card = "";
            CardWithReader = "";
            NoMedium = "";

            switch (medium)
            {
                case "USB_TOKEN":
                    Token = @"X";
                    break;
                case "SMART_CARD_WITH_READER":
                    CardWithReader = @"X";
                    break;
                case "SMART_CARD":
                    Card = @"X";
                    break;
                default:
                    NoMedium = "X";
                    break;
            }
        }

        private void SetExpiry(string expiry)
        {
            switch (expiry)
            {
                case "365":
                    Expiry = @"1";
                    break;
                case "730":
                    Expiry = @"2";
                    break;
                case "1095":
                    Expiry = @"3";
                    break;
                case "1462":
                    Expiry = @"4";
                    break;
                case "1825":
                    Expiry = @"5";
                    break;
                default:
                    Expiry = @"";
                    break;
            }
        }
    }
}
