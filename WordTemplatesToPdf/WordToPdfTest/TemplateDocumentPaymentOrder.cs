﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WordToPdf;

namespace WordToPdfTest
{
    public class TemplateDocumentPaymentOrder : TemplateDocumentSerbianPost
    {
        public string BrojZahteva { get; set; }
        public string DatumPodnosenjaZahteva { get; set; }
        public string Cena { get; set; }
        public string Platilac { get; set; }
        public string SvrhaUplate { get; set; }

        public TemplateDocumentPaymentOrder(string requestNumber, string price, string firstName, string lastName, string street, string streetNo, string postNo, string place, string purposeOfPayment, bool savePdfAlso = true)
            : base()
        {
            DocumentType = TemplateDocumentType.PaymentOrder;
            BrojZahteva = requestNumber;
            DatumPodnosenjaZahteva = Utils.GetShortSerbianDate(DateTime.Now, true);
            Cena = price;
            Platilac = firstName + " " + lastName + ", " + street + " " + streetNo + ", " + postNo + " " + place;
            SvrhaUplate = GetPurposeOdPayment(purposeOfPayment) + @" (" + requestNumber + @")";
            ContractDateTime = GetContractDateTime(requestNumber);

            CreateDocumentName(savePdfAlso);

            SetTemplate();
            
            Bookmarks.Add(new Bookmark(@"BrojZahteva", BrojZahteva));
            Bookmarks.Add(new Bookmark(@"DatumPodnosenjaZahteva", DatumPodnosenjaZahteva));
            Bookmarks.Add(new Bookmark(@"Cena", Cena));
            Bookmarks.Add(new Bookmark(@"Cena1", Cena));
            Bookmarks.Add(new Bookmark(@"Platilac", Platilac));
            Bookmarks.Add(new Bookmark(@"SvrhaUplate", SvrhaUplate));
        }

        private string GetPurposeOdPayment(string purposeString)
        {
            //ova funkcija vraca string koji treba da bude na nalogu za uplatu
            /*
                moguce vrednosti ISSUING ili UNBLOCKING
             */
            string ret = string.Empty;
            switch (purposeString)
            {
                case "ISSUING":
                    ret = @"Накнада за издавање сертификата" ;
                    break;
                case "UNBLOCKING":
                    ret = @"Накнада за деблокаду смарт картице/USB токена";
                    break;
                default:
                    break;
            }
            return ret;
        }

        public override string CreateWordDocumentFromTemplate()
        {
            return string.Empty;
            //if (mutex.WaitOne(10000))  //ovo moze da radi samo jedan po jedan. Blokiraj maksimalno 10 sekundi
            //{
            //    Microsoft.Office.Interop.Word.Application wordApp = null;
            //    Microsoft.Office.Interop.Word.Document doc = null;

            //    string tempTemplateFile = string.Empty; //privremeni template fajl sa kojim cu da radim da se slucajno ne bi nesto desilo pravom template fajlu
            //    try
            //    {
            //        wordApp = new Microsoft.Office.Interop.Word.Application()
            //        {
            //            Visible = false
            //        };

            //        //kreiraj privremeni fajl i kopiraj template fajl u njega
            //        tempTemplateFile = Utils.CreateTempFile();
            //        File.Copy(Template.FullName, tempTemplateFile, true);

            //        object wordTemplateDocumentObj = (object)tempTemplateFile;

            //        doc = wordApp.Documents.Open(ref wordTemplateDocumentObj);

            //        //ubaci vrednosti za bookmark-e
            //        foreach (Bookmark bkm in Bookmarks)
            //        {
            //            doc.Bookmarks.get_Item(GetBookmarkByName(doc.Bookmarks, bkm.Name)).Range.Text = bkm.Value;
            //        }

                    
            //        doc.SaveAs(FullName);

            //        if (PdfSaveDocumentAsFullPath != "")
            //        {
            //            doc.ExportAsFixedFormat(PdfSaveDocumentAsFullPath, Microsoft.Office.Interop.Word.WdExportFormat.wdExportFormatPDF);
            //        }

            //        ((Microsoft.Office.Interop.Word._Document)doc).Close(false);
            //        ((Microsoft.Office.Interop.Word._Application)(wordApp.Application)).Quit();

            //    }
            //    catch (Exception ex)
            //    {
            //        throw new Exception("Error creating documents. " + ex.Message);
            //    }
            //    finally
            //    {
            //        try
            //        {
            //            ReleaseResource(wordApp);
            //            ReleaseResource(doc);
            //            mutex.ReleaseMutex();
            //            Utils.DeleteFile(tempTemplateFile);
            //        }
            //        catch (Exception)
            //        {
            //            //nije uspelo oslobadjanje resursa, necu nista da radim
            //        }
            //    }

            //    if (PdfSaveDocumentAsFullPath != "")
            //    {
            //        //ako je snimljen pdf, vrati njega kao rezultat
            //        return PdfSaveDocumentAsFullPath;
            //    }
            //    else
            //    {
            //        //ako nije snimljen pfd, vrati link ka word dokumentu
            //        return FullName;
            //    }
            //}
            //else
            //{
            //    throw new Exception("Thread busy. Document was not created.");
            //}
        }
    }
}
